#pragma once

#include <Windows.h>
#include <TlHelp32.h>
#include <iostream>
#include <string>
#include <Psapi.h>
#pragma comment(lib, "psapi")

class Memory {
private:
	DWORD m_clientDLL;
	DWORD m_baseDLL;
	DWORD m_engineDLL;
	HANDLE m_processHandle;
	int m_iProcessID;

	DWORD GetModuleBaseAddress (std::string lpszModuleName )
	{
		HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPMODULE, m_iProcessID );
		DWORD dwModuleBaseAddress = 0;
		if ( hSnapshot != INVALID_HANDLE_VALUE )
		{
			MODULEENTRY32 ModuleEntry32 = { 0 };
			ModuleEntry32.dwSize = sizeof ( MODULEENTRY32 );
			if ( Module32First ( hSnapshot, &ModuleEntry32 ) )
			{
				do
				{
					if ( strcmp ( ModuleEntry32.szModule, lpszModuleName.c_str() ) == 0 )
					{
						dwModuleBaseAddress = ( DWORD ) ModuleEntry32.modBaseAddr;
						break;
					}
				} while ( Module32Next ( hSnapshot, &ModuleEntry32 ) );
			}
			CloseHandle ( hSnapshot );
		}
		return dwModuleBaseAddress;
	}

	int GetProcessId ( char* processName ) {
		SetLastError ( 0 );
		PROCESSENTRY32 pe32;
		HANDLE hSnapshot = NULL;
		GetLastError ( );
		pe32.dwSize = sizeof ( PROCESSENTRY32 );
		hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );

		if ( Process32First ( hSnapshot, &pe32 ) ) {
			do {
				if ( strcmp ( pe32.szExeFile, processName ) == 0 )
					break;
			} while ( Process32Next ( hSnapshot, &pe32 ) );
		}

		if ( hSnapshot != INVALID_HANDLE_VALUE )
			CloseHandle ( hSnapshot );
		int err = GetLastError ( );
		//std::cout << err << std::endl;
		if ( err != 0 )
			return 0;
		return pe32.th32ProcessID;
	}

	BOOL SetPrivilege ( HANDLE hToken, LPCTSTR lpszPrivilege, BOOL bEnablePrivilege )
	{
		TOKEN_PRIVILEGES tp;
		LUID luid;

		if ( !LookupPrivilegeValue ( NULL, lpszPrivilege, &luid ) ) {
			//printf("LookupPrivilegeValue error: %u\n", GetLastError() );
			return FALSE;
		}

		tp.PrivilegeCount = 1;
		tp.Privileges [ 0 ].Luid = luid;
		if ( bEnablePrivilege )
			tp.Privileges [ 0 ].Attributes = SE_PRIVILEGE_ENABLED;
		else
			tp.Privileges [ 0 ].Attributes = 0;

		if ( !AdjustTokenPrivileges ( hToken, FALSE, &tp, sizeof ( TOKEN_PRIVILEGES ), ( PTOKEN_PRIVILEGES ) NULL, ( PDWORD ) NULL ) ) {
			//printf("AdjustTokenPrivileges error: %u\n", GetLastError() );
			return FALSE;
		}

		if ( GetLastError ( ) == ERROR_NOT_ALL_ASSIGNED ) {
			//printf("The token does not have the specified privilege. \n");
			return FALSE;
		}

		return TRUE;
	}



	BOOL GetDebugPrivileges ( void ) {
		HANDLE hToken = NULL;
		if ( !OpenProcessToken ( GetCurrentProcess ( ), TOKEN_ADJUST_PRIVILEGES, &hToken ) )
			return FALSE; //std::cout << "OpenProcessToken() failed, error\n>> " << GetLastError() << std::endl;
						  //else std::cout << "OpenProcessToken() is OK, got the handle!" << std::endl;

		if ( !SetPrivilege ( hToken, SE_DEBUG_NAME, TRUE ) )
			return FALSE; //std::cout << "Failed to enable privilege, error:\n>> " << GetLastError() << std::endl;

		return TRUE;
	}

public:

	DWORD getEngine ( ) {
		return m_engineDLL;
	}

	DWORD getClient ( ) {
		return m_clientDLL;
	}
	
	DWORD getBase ( ) {
		return m_baseDLL;
	}

	HANDLE getProcessHandle ( ) {
		return m_processHandle;
	}

	template <class T>
	T read ( DWORD addr )
	{
		T buffer;
		SIZE_T bytesRead;
		ReadProcessMemory ( m_processHandle, ( LPCVOID ) addr, &buffer, sizeof ( T ), &bytesRead);
		return buffer;
	}

	template <class T>
	bool write ( DWORD addr, T newValue ) {
		return WriteProcessMemory ( m_processHandle, ( LPVOID )addr, &newValue, sizeof ( T ), 0 );
	}


	bool initialise ( char* processname )
	{
		/* !~ Find process ~! */
		if ( m_processHandle == NULL )
		{
			auto processId = GetProcessId ( processname );
			if ( processId != 0 ) {
				m_iProcessID = processId;
				//process id found
				m_processHandle = OpenProcess ( PROCESS_ALL_ACCESS, FALSE, processId );
				std::string base = processname;
				std::string engine = "engine.dll";
				std::string client = "client_panorama.dll";
				m_engineDLL = GetModuleBaseAddress ( engine );
				m_clientDLL = GetModuleBaseAddress ( client );
				std::cout << std::hex << m_engineDLL << std::endl;
				std::cout << std::hex << m_clientDLL << std::endl;
				std::cout << "Everything wen't correct" << std::endl;
				return true;
			}
			else {
				std::cout << "Couldn't find processID!" << std::endl;
				return false;
			}

		}

		return false;
	}

	void dip ( ) {
		CloseHandle ( m_processHandle );
	}

};
extern Memory* memory;
