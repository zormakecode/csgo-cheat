#include <Windows.h>
#include "Memory.h"
#include "Offsets.hpp"
#include <iostream>

#include "imgui/imgui.h"
#include "imgui/dx9/imgui_impl_dx9.h"
#include "imgui/dx9/imgui_impl_win32.h"
#include <d3d9.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include <random>
#include <tchar.h>

bool chamsEnabled;
float chamsColor[3] = {1,0.2,0.2};
DWORD clRender = 0x0070;

struct RGBA {
	int r;
	int g;
	int b;
	int a;
};

void chamsThread ( ) {

	while ( true ) { 
	if ( chamsEnabled )
	{
		DWORD localPlayer = memory->read<DWORD> ( memory->getClient ( ) + hazedumper::signatures::dwLocalPlayer );
		int local_team = memory->read<int> ( localPlayer + hazedumper::netvars::m_iTeamNum );

		if ( localPlayer == NULL )
			return;

		for ( int i = 0; i < 65; i++ )
		{
			DWORD entity = memory->read<DWORD> ( memory->getClient ( ) + hazedumper::signatures::dwEntityList + ( i * 0x10 ) );
			if ( entity != NULL )
			{

				int entity_team = memory->read<int> ( entity + hazedumper::netvars::m_iTeamNum );

				if ( entity_team == local_team )
					continue;

				RGBA color = { chamsColor [ 0 ] * 255, chamsColor [ 1 ] * 255, chamsColor [ 2 ] * 255, 255 };
				memory->write<RGBA> ( entity + clRender, color );
			}
		}
	}
	Sleep ( 10 );
	}
}

static LPDIRECT3DDEVICE9        g_pd3dDevice = NULL;
static D3DPRESENT_PARAMETERS    g_d3dpp;

extern LRESULT ImGui_ImplWin32_WndProcHandler ( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
LRESULT WINAPI WndProc ( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	if ( ImGui_ImplWin32_WndProcHandler ( hWnd, msg, wParam, lParam ) )
		return true;

	switch ( msg )
	{
	case WM_SIZE:
		if ( g_pd3dDevice != NULL && wParam != SIZE_MINIMIZED )
		{
			ImGui_ImplDX9_InvalidateDeviceObjects ( );
			g_d3dpp.BackBufferWidth = LOWORD ( lParam );
			g_d3dpp.BackBufferHeight = HIWORD ( lParam );
			HRESULT hr = g_pd3dDevice->Reset ( &g_d3dpp );
			if ( hr == D3DERR_INVALIDCALL )
				IM_ASSERT ( 0 );
			ImGui_ImplDX9_CreateDeviceObjects ( );
		}
		return 0;
	case WM_SYSCOMMAND:
		if ( ( wParam & 0xfff0 ) == SC_KEYMENU ) // Disable ALT application menu
			return 0;
		break;
	case WM_DESTROY:
		PostQuitMessage ( 0 );
		return 0;
	}
	return DefWindowProc ( hWnd, msg, wParam, lParam );
}

std::string random_string ( std::string::size_type length )
{
	static auto& chrs = "0123456789"
		"abcdefghijklmnopqrstuvwxyz"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	thread_local static std::mt19937 rg { std::random_device { }( ) };
	thread_local static std::uniform_int_distribution<std::string::size_type> pick ( 0, sizeof ( chrs ) - 2 );

	std::string s;

	s.reserve ( length );

	while ( length-- )
		s += chrs [ pick ( rg ) ];

	return s;
}

int createWindow ( )
{
	std::string classname = random_string ( 12 );
	std::string windowname = random_string ( 10 );
	std::string formname = random_string ( 24 );
	// Create application window
	WNDCLASSEX wc = { sizeof ( WNDCLASSEX ), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle ( NULL ), NULL, NULL, NULL, NULL, _T ( classname.c_str() ), NULL };
	RegisterClassEx ( &wc );
	HWND hwnd = CreateWindow ( _T ( classname.c_str() ), _T ( windowname.c_str() ), ( WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX ) , 100, 100, 500, 500, NULL, NULL, wc.hInstance, NULL );

	// Initialize Direct3D
	LPDIRECT3D9 pD3D;
	if ( ( pD3D = Direct3DCreate9 ( D3D_SDK_VERSION ) ) == NULL )
	{
		UnregisterClass ( _T ( classname.c_str() ), wc.hInstance );
		return 0;
	}
	ZeroMemory ( &g_d3dpp, sizeof ( g_d3dpp ) );
	g_d3dpp.Windowed = TRUE;
	g_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	g_d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	g_d3dpp.EnableAutoDepthStencil = TRUE;
	g_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	g_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE; // Present with vsync
															//g_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE; // Present without vsync, maximum unthrottled framerate

															// Create the D3DDevice
	if ( pD3D->CreateDevice ( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hwnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &g_d3dpp, &g_pd3dDevice ) < 0 )
	{
		pD3D->Release ( );
		UnregisterClass ( _T ( classname.c_str() ), wc.hInstance );
		return 0;
	}

	// Setup Dear ImGui binding
	IMGUI_CHECKVERSION ( );
	ImGui::CreateContext ( );
	ImGuiIO& io = ImGui::GetIO ( ); ( void ) io;
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
	ImGui_ImplWin32_Init ( hwnd );
	ImGui_ImplDX9_Init ( g_pd3dDevice );

	// Setup style
	ImGui::StyleColorsDark ( );
	//ImGui::StyleColorsClassic();

	// Load Fonts
	// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them. 
	// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple. 
	// - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
	// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
	// - Read 'misc/fonts/README.txt' for more instructions and details.
	// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
	//io.Fonts->AddFontDefault();
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
	//ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
	//IM_ASSERT(font != NULL);

	bool show_demo_window = true;
	bool show_another_window = false;
	int current_tab = 0;
	ImVec4 clear_color = ImVec4 ( 0.45f, 0.55f, 0.60f, 1.00f );
	CreateThread ( 0, 0, ( LPTHREAD_START_ROUTINE ) chamsThread, 0, 0, 0 );

	// Main loop
	MSG msg;
	ZeroMemory ( &msg, sizeof ( msg ) );
	ShowWindow ( hwnd, SW_SHOWDEFAULT );
	UpdateWindow ( hwnd );
	while ( msg.message != WM_QUIT )
	{
		// Poll and handle messages (inputs, window resize, etc.)
		// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
		// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
		// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
		// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
		if ( PeekMessage ( &msg, NULL, 0U, 0U, PM_REMOVE ) )
		{
			TranslateMessage ( &msg );
			DispatchMessage ( &msg );
			continue;
		}

		// Start the Dear ImGui frame
		ImGui_ImplDX9_NewFrame ( );
		ImGui_ImplWin32_NewFrame ( );
		ImGui::NewFrame ( );

		ImGui::SetNextWindowPos ( ImVec2 ( -10, -20 ) );
		ImGui::SetNextWindowSize ( ImVec2 ( 500, 500 ) );

		ImGui::Begin ( formname.c_str ( ), NULL, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse );
		{
			ImGui::SetCursorPos ( ImVec2 ( 0, 20 ) );
			if ( ImGui::Button ( "Main", ImVec2 ( 250, 30 ) ) ) {
				current_tab = 0;
			}
			ImGui::SetCursorPos ( ImVec2 ( 250, 20 ) );
			if ( ImGui::Button ( "Colors", ImVec2 ( 250, 30 ) ) ) {
				current_tab = 1;
			}

			switch ( current_tab ) {
			case 0:
				ImGui::Checkbox ( "Chams Enabled", &chamsEnabled );
				break;
			case 1:
				ImGui::ColorPicker3 ( "", chamsColor );
				break;
			default:
				ImGui::Text ( "wait what" );
				break;
			}
		}
		ImGui::End ( );

		// Rendering
		ImGui::EndFrame ( );
		g_pd3dDevice->SetRenderState ( D3DRS_ZENABLE, false );
		g_pd3dDevice->SetRenderState ( D3DRS_ALPHABLENDENABLE, false );
		g_pd3dDevice->SetRenderState ( D3DRS_SCISSORTESTENABLE, false );
		D3DCOLOR clear_col_dx = D3DCOLOR_RGBA ( ( int ) ( clear_color.x*255.0f ), ( int ) ( clear_color.y*255.0f ), ( int ) ( clear_color.z*255.0f ), ( int ) ( clear_color.w*255.0f ) );
		g_pd3dDevice->Clear ( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, clear_col_dx, 1.0f, 0 );
		if ( g_pd3dDevice->BeginScene ( ) >= 0 )
		{
			ImGui::Render ( );
			ImGui_ImplDX9_RenderDrawData ( ImGui::GetDrawData ( ) );
			g_pd3dDevice->EndScene ( );
		}
		HRESULT result = g_pd3dDevice->Present ( NULL, NULL, NULL, NULL );

		// Handle loss of D3D9 device
		if ( result == D3DERR_DEVICELOST && g_pd3dDevice->TestCooperativeLevel ( ) == D3DERR_DEVICENOTRESET )
		{
			ImGui_ImplDX9_InvalidateDeviceObjects ( );
			g_pd3dDevice->Reset ( &g_d3dpp );
			ImGui_ImplDX9_CreateDeviceObjects ( );
		}
	}
	memory->dip ( );
	ImGui_ImplDX9_Shutdown ( );
	ImGui_ImplWin32_Shutdown ( );
	ImGui::DestroyContext ( );

	if ( g_pd3dDevice ) g_pd3dDevice->Release ( );
	if ( pD3D ) pD3D->Release ( );
	DestroyWindow ( hwnd );
	UnregisterClass ( _T ( classname.c_str() ), wc.hInstance );

}


/* !~ Entry Point of prorgram ~! */
int main ( ) {
	std::string processname = "csgo.exe";
	if ( memory->initialise ( (char*) processname.c_str() ) )
	{
		std::cout << "found csgo" << std::endl;
		CreateThread ( 0, 0, ( LPTHREAD_START_ROUTINE ) createWindow, 0, 0, 0 );
	}
	else {
		CreateThread ( 0, 0, ( LPTHREAD_START_ROUTINE ) createWindow, 0, 0, 0 );
		std::cin.get ( );
	}

	while ( true )
	{
		Sleep ( 1 );
	}

	return 0;

}